#!/usr/bin/env python3

# MIT License
#
# Copyright (c) 2022 contact@t3Y.eu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PIL import Image
import socket
import time

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

FPS = 30
FRAME_TIME = 1000 / FPS
FRAMES = 6572
WIDTH = 176
HEIGHT = 132 // 2
counter = 1

ascii = []
for i in range(HEIGHT):
    line = []
    for j in range(WIDTH):
        line.append(f"{i}{j}")
    ascii.append(line)


timestamp = time.time_ns()
while counter <= FRAMES:
    frame = Image.open(f"./bin/frames/img{str(counter).zfill(4)}.png")
    frame = frame.resize((WIDTH, HEIGHT * 2), Image.ANTIALIAS)
    # frame = frame.tobytes()
    for i in range(HEIGHT):
        for j in range(WIDTH):
            ascii[i][j] = "#"
            pixel = sum(frame.getpixel((j, i * 2))) + sum(
                frame.getpixel((j, i * 2 + 1))
            )
            if pixel <= 283:
                ascii[i][j] = " "

    payload = bytes("\0" + "\0".join(["".join(line) for line in ascii]), "utf8")
    sock.sendto(payload, ("localhost", 54321))
    prep_time = (time.time_ns() - timestamp) / 1000000
    time.sleep(max(0, (FRAME_TIME - prep_time) / 1000))
    timestamp = time.time_ns()

    counter += 1
