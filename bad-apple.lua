-- MIT License
-- 
-- Copyright (c) 2022 contact@t3Y.eu
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

bad_apple_protocol = Proto("Bad-Apple", "Bad Apple Transport Protocol")

line = ProtoField.string("line", ":^)", base.ASCII)

bad_apple_protocol.fields = {line}

function bad_apple_protocol.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = "Bad Apple!!"
    local done = false
    local start = 0
    local index = 0
    local apple_tree = tree:add(bad_apple_protocol, buffer(), "(^v^)")
    while (not done) do
        local byte = buffer(index, 1):uint()
        if byte == 0 then
            if start ~= index then
                apple_tree:add(line, buffer(start+1, index - start - 1))
                start = index
            end
        end
        index = index + 1
        if index == buffer:len() then
            done = true
            apple_tree:add(line, buffer(start+1, index - start - 1))
        end
    end
end

local udp_port = DissectorTable.get("udp.port")
udp_port:add(54321, bad_apple_protocol)
