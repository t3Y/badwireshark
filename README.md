Script and dissector to display Bad Apple!! (or any 1-bit video really) in Wireshark. You can see it in action [here](https://www.youtube.com/watch?v=5gSg72KW1TY)

The script looks for frames `0001.png - xxxx.png` under `./bin/frames/`, which you will need to have extracted with something like ffmpeg beforehand (e.g. `ffmpeg -i bad-apple.webm -vf fps=30 bin/frames/img%04d.png`) and then sends each frame as ASCII to the specified address (localhost by default). Mind the MTU of whatever interface you want to send on though, frames get big fast.

The Wireshark dissector should be placed in your Wireshark local plugin folder (or symlinked).
